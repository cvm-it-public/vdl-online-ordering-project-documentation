# VDL Online Ordering Project Documentation



## Overview

All requests are made to the url:	https://vdl.msu.edu/WebAPI/

Requests are submitted using https with the GET or POST commands.  Valid responses are presented using the XML format.

A request will generate one of the following response codes.

|  Code  |  Description  |
|  ----  |  -----------  |
| 200    | OK            |
| 400    | Bad Request - your request is not correct |
| 401    | Unauthorized - the WebID and Token pair is invalid |
| 500    | Internal server error - contact VDL IT for support |

## Authentication

Access to VDL WebAPI data requires a token, which is generated via an existing VDL account and password.  A token is generated using the url:<br/>
`https://vdl.msu.edu/WebAPI/LoginXML.exe`<br/>
` ?WebID={id}`<br/>
` &Password={pass}`

|  Parameter  |  Type  |  Required  |  Description  |
|  ---------  |  ----  |  --------  |  -----------  |
| id          | string | [X]        | VDL client account number |
| pass        | string | [X]        | VDL client password - needs escape coding |

### Sample Request
```
GET /WebAPI/LoginXML.exe HTTP/2
Host: vdl.msu.edu
Content-Type: application/x-www-form-urlencoded
WebID=1234&Password=12password34
```
### Sample Response
```
<?xml version="1.0"?>
<Data>
  <Login>
    <WebID>1234</WebID>
    <Name>Main Street Animal Clinic</Name>
    <Token>5D14BBFF9C994EF0</Token>
    <IPAddr>35.9.100.100</IPAddr>
  </Login>
</Data>
```
|  Field  |  Type  |  Description  |
|  -----  |  ----  |  -----------  |
|  Login  |  container  |  Successful login contents  |
|  WebID  |  string  |  VDL client account number  |
|  Name   |  string  |  VDL client account name  |
|  Token  |  string  |  16-character token  |
|  IPAddr |  IPv4    |  Client-side IP address  |

Note: the token will expire if the session is idle for more than 10 minutes.

## Catalog Listing
The list of Test Catalog entries is retrieved by using the URL:<br/>
`https://vdl.msu.edu/WebAPI/CatalogXML.exe`<br/>
` ?User={id}`<br/>
` &Token={token}`<br/>
` &Name={name}`<br/>
` &Species={species}`<br/>
` &Sort={sort}`

|  Parameter  |  Type  |  Required  |  Description  |
|  ---------  |  ----  |  --------  |  -----------  |
|  id         |  string  |  [X]  |  VDL client account number  |
|  token      |  string  |  [X]  |  Authentication token  |
|  name       |  string  |       |  Full or partial match to test name or alternate strings  |
|  species    |  string  |       |  See table below  |
|  sort       |  string  |       |  Column title: "TestCode", "Name", "CernerCode", "View", "Order"  |

The database is queried for matching records based on the search parameters {name, species} and returned in the order {sort}.

Species Options:<br/>
|  "Alpaca"     |  "Canine"         |  "Ferret"     |  "Mustelid"      |  "Porcine"  |
|  "Amphibian"  |  "Caprine"        |  "Human"      |  "Non Specific"  |  "Primate"  |
|  "Aquatic"    |  "Cervidae"       |  "Insect"     |  "Non-Animal"    |  "Reptile"  |
|  "Arthropod"  |  "Environmental"  |  "Lagomorph"  |  "Other"         |  "Rodent"   |
|  "Avian"      |  "Equine"         |  "Llama"      |  "Other Mammal"  |  "Unknown"  |
|  "Bovine"     |  "Feline"         |  "Mink"       |  "Ovine"         |             |

Sample Request:<br/>
```
GET /WebAPI/CatalogXML.exe HTTP/2
Host: vdl.msu.edu
Content-Type: application/x-www-form-urlencoded
User=1234&Token=5D14BBFF9C994EF0&Name=Cort&Species=Bovine
```
Sample Response:<br/>
```
<?xml version="1.0"?>
<Data>
  <List>
    <Item>
      <TestCode>1637</TestCode>
      <CernerCode>20017</CernerCode>
      <Name>Cortisol, Baseline</Name>
      <View></View>
      <Order>True</Order>
    </Item>
    <Item>...</Item>
    <Item>...</Item>
    <Item>...</Item>
  </List>
</Data>
```
| Field | Type | Repeat | Description |
| ----- | ---- | ------ | ----------- |
| List | container |    | List of tests |
| Test | container | [X] | One test |
| TestCode | integer |   | Identifier test code |
| CernerCode | string |   | External order code for lab system |
| Name | string |   | Test name |
| View | char |   | blank=everyone, 'V'=VTH, 'I'=Internal |
| Order | boolean |   | True/False |

Multiple items may be returned in the XML document. Users external to the CVM should only see the `<Name>` and `<CernerCode>`.

## Test Item
A detailed entry for a single test item is retrieved using the url:<br/>
`https://vdl.msu.edu/WebAPI/CatalogXML.exe`<br/>
` ?User={id}`<br/>
` &Token={token}`<br/>
` &Test={test}`<br/>

| Paramter | Type | Required | Description |
|---|---|---|---|
| id | string | [X] | VDL client account number |
| token | string | [X] | Authentication token |
| test | integer |   | Identifier test code from Catalog Listing |

Sample Request:<br/>
```
GET /WebAPI/CatalogXML.exe HTTP/2
Host: vdl.msu.edu
Content-Type: application/x-www-form-urlencoded
User=1234&Token=5D14BBFF9C994EF0&Test=1637
```
Sample Response:<br/>
```
<?xml version="1.0"?>
<Data>
  <Test>
    <Information>
      <TestCode>1637</TestCode>
      <CernerCode>20017</CernerCode>
      <OrderCode>671965</OrderCode>
      <OrderFlag></OrderFlag>
      <Name>Cortisol, Baseline</Name>
      <Section>Endocrinology</Section>
      <Price>$20.00</Price>
      <Turnaround>1-3 Days</Turnaround>
      <DaysRun>Mon, Tue, Wed, Thu, Fri</DaysRun>
      <Species>Most Species</Species>
    </Information>
    <Samples>
      <Title>Any of the Following Specimens are Okay</Title>
      <Item>
        <Specimen>Serum</Specimen>
        <Container>Leakproof Tube</Container>
        <Volume>0.5 mL</Volume>
        </Item>
    </Samples>
    <Note>
      <Title>Collection Protocol</Title>
      <Text>DO NOT SEND WHOLE BLOOD</Text>
    </Note>
    <Note>...</Note>
    <Note>...</Note>
    <Note>...</Note>
  </Test>
</Data>
```

| Field | Type | Repeat | Description |
|---|---|---|---|
| Test | container |   | One Test |
| Information | container |   | Ordering information |
| TestCode | integer |   | Identifier test code |
| CernerCode | string |   | External order code for lab system |
| OrderCode | integer |   | Internal order code for lab system |
| OrderFlag | boolean |   | True/False  |
| Name | string |   | Test name |
| Section | string |   | Lab section |
| Price | string |   | '$' + float |
| Turnaround | string |   | Average time to complete test |
| DaysRun | string |   | Days of week the test is run |
| Species | string |   | Species for which test is valid |
| Samples  | container  |   | Appropriate sample submissions |
| Title | string |   | Informational title |
| Item | container | [X] | Sample entry |
| Specimen | string |   | Tissue, fluid, feed, etc. |
| Container | string |   | Preferred container for specimen |
| Volume | string |   | Minimum sample size |
| Note | container  | [X] | Informational note |
| Title | string  |   | Note header |
| Text | string |     | Note contents |

Each test has one Information section, one Samples section with multiple items, and multiple Note sections.